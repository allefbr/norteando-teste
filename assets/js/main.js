$(function(){

    // Picture element HTML5 shiv
    document.createElement( "picture" );
});

$(document).ready(function(){
    $el = $('.navbar, .navbar-nav li');
    $bt = $('.at-menu');
    
    function checkClassHidden(){
        if( $($el).hasClass('hidden-xs') ){
            $($el).removeClass('hidden-xs');
        } else {
            $($el).addClass('hidden-xs');
        };
    }
    
    $( $bt, $el ).click(function(){
       checkClassHidden();
   });
});