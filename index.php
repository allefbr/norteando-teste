<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="initial-scale=1" />
        <title>Norteando - Teste de Nível</title>
        <link rel="icon" href="build/images/icons/favicon.png" />
        <!-- Fontes -->
        <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        
        <!-- Estilo -->
        <link rel="stylesheet" href="build/css/style.css" />
        
        
    </head>
    
    
    <body>
       <div class="container">
           <header class="topo col-lg-12">
               <div class="topo-banner">
                   <h1 class="col-lg-3 col-md-3 col-xs-4 center-block"><a href="#" class="logo">Norteando</a></h1>
                   <a href="#" class="col-lg-8 col-lg-offset-1 col-md-7 col-md-offset-2 col-sm-12 col-xs-12 banner">
                        <picture>
                            <source media="(min-width: 1200px)" srcset="build/images/banner-topo.jpg" />
                            <source media="(min-width: 992px)" srcset="build/images/banner-topo-m.jpg" />
                            <img srcset="build/images/banner-topo.jpg" alt="Banner" />
                        </picture> 
                    </a>   
               </div>

               <div class="row pos-r">
                   <span class="hidden-lg hidden-md hidden-sm at-menu col-xs-1">Ativa Menu</span>
                   
                   <nav class="col-lg-10 col-md-9 col-sm-9 col-xs-12 hidden-xs navbar navbar-default" role="navigation">
                       <div class="container pad-reset-xs">
                          <ul class="nav navbar-nav">
                               <li class="hidden-xs"><a href="#">Coaching</a></li>
                               <li class="hidden-xs"><a href="#">Colunas</a></li>
                               <li class="hidden-xs"><a href="#">Estilo & Beleza</a></li>
                               <li class="hidden-xs"><a href="#">Esporte</a></li>
                               <li class="hidden-xs"><a href="#">Negócios</a></li>
                               <li class="hidden-xs hidden-sm"><a href="#">Meio Ambiente</a></li>
                               <li class="hidden-xs hidden-md hidden-sm"><a href="#">Entretenimento</a></li>
                               <li class="hidden-xs hidden-md hidden-sm"><a href="#">Turismo</a></li>
                               <li class="hidden-xs"><a href="#" class="plus">Mais Detalhes</a></li>
                           </ul>
                       </div>
                    </nav>

                    <form class="col-lg-2 col-md-3 col-sm-3 col-xs-11 box-form box-form-busc" role="form">
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Buscar">
                        <button type="submit" class="btn bt-busc">Buscar</button>
                    </form>
                </div>
            </header>

            <main>
                <!-- Conteudo Geral -->
                <article class="not-topo">
                    <section class="row">
                        <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                            <ul class="box-not">
                                <li class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <a href="#" title="">
                                        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12 box-img">
                                            <img src="build/images/arte/gemeos.jpg" alt="" />
                                            <span class="tag-violet">Arte</span>
                                        </div>

                                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12 txt pad-prin">
                                            <h1 class="tit1 tit-blue">'Período de Copa sempre foi bom para pintar na rua', dizem Osgêmeos</h1>
                                            <p class="txt-desc">Gustavo e Otávio Pandolfo abrem exposição 'A ópera da Lua' em SP. Entre as obras da mostra está a maior escultura já realizada por eles.</p>
                                        </div>
                                    </a>
                                </li>
                                
                                <li class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    <a href="#" title="">
                                        <div class="col-lg-12 col-md-12 col-sm-7 box-img ">
                                            <img src="build/images/cinema/filme-junto-e-misturado.jpg" alt="" />
                                            <span class="tag-violet">Cinema</span>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-5 txt pad-prin">
                                            <h2 class="tit2 tit-blue">Estreia: 'Juntos e misturados' destaca humor exagerado de Adam Sandler</h2>
                                            <p class="txt-desc pad-b bor-bot">Ele se junta a Drew Barrymore em comédia pela terceira vez na carreira. </p>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 pad-princ-custom">
                            <ul class="list-p">
                                <li class="col-lg-12 col-md-5 col-sm-5 col-xs-12 box-border">
                                    <a href="#" title="">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7 box-img">
                                            <img src="build/images/musica/beyonce.jpg" alt="" />
                                            <span class="tag-violet">Música</span>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-5 txt pad-reset-r">
                                            <h3 class="tit3">Beyoncé lidera indicações ao MTV VMA 2014. </h3>
                                            <p class="txt-desc">Veja lista completa </p>
                                        </div>
                                    </a>
                                </li>
                                
                                <li class="col-lg-12 col-lg-offset-0 col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2 col-xs-12 box-border">
                                    <a href="#" title="">
                                        <div class="col-lg-6 col-md-5 col-sm-5 col-xs-6 box-img">
                                            <img src="build/images/musica/Keira-Knightley.jpg" alt="" />
                                            <span class="tag-violet">Música</span>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-5 txt pad-reset-r">
                                            <h3 class="tit3">Keira Knightley e Mark Ruffalo gravam disco para filme 'Mesmo se Nada der Certo'</h3>
                                            <p class="txt-desc">Confira o trailler</p>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>            
                    </section>

                    <!-- Estilo & Beleza -->
                    <section class="row block-not">
                        <div class="col-lg-9 col-md-12 col-sm-12">
                            <h1 class="tit-dest pink">Estilo & Beleza</h1>

                            <ul class="box-not">
                                <li class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="#" title="">
                                        <div class="col-lg-12 box-img">
                                            <img src="build/images/estilo/orange-is-the-new-black-glamurosas.jpg" alt="" />
                                            <span class="tag tag-pink">Estilo</span>
                                        </div>

                                        <div class="col-lg-12 txt box-border">
                                            <h3 class="tit3">Veja as atrizes de Orange is the New Black glamurosas no tapete vermelho do Emmy</h3>
                                        </div>
                                    </a>
                                </li>
                                
                                 <li class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="#" title="">
                                        <div class="col-lg-12 box-img">
                                           <img src="build/images/moda/verde-e-amarelo.jpg" alt="" />
                                            <span class="tag tag-pink">moda</span>
                                        </div>

                                        <div class="col-lg-12 txt box-border">
                                            <h3 class="tit3">Como usar verde e amarelo para torcer pelos Brasil</h3>
                                        </div>
                                    </a>
                                </li>
                                
                                <li class="col-lg-4 col-md-4 hidden-sm">
                                    <a href="#" title="">
                                        <div class="col-lg-12 box-img">
                                            <img src="build/images/moda/look-gravidas.jpg" alt="" />
                                            <span class="tag tag-pink">moda</span>
                                        </div>

                                        <div class="col-lg-12 txt box-border">
                                            <h3 class="tit3">Dicas de look para as grávidas fashionistas</h3>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-lg-3 col-md-12 col-sm-12 box-cinema">
                            <a href="#" title="">
                                <picture>
                                    <source media="(min-width: 1200px)" srcset="build/images/cinema/chamada-filmes.jpg" />
                                    <source media="(min-width: 992px)" srcset="build/images/cinema/chama-filmes-m.jpg" />
                                    <source media="(min-width: 750px)" srcset="build/images/cinema/chama-filmes-m.jpg" />
                                    <img srcset="build/images/cinema/chamada-filmes.jpg" alt="Banner" class="img-full" />
                                </picture>

                                <div class="text-center cinema-info">
                                    <h1 class="tit1">• Cinema •</h1>
                                    <h2 class="tit2">Estreias da Semana</h2>
                                </div>
                            </a>
                        </div>
                    </section>

                    <!-- Gastronomia -->
                    <section class="row block-not">
                        <h1 class="col-lg-12 col-md-12 col-sm-12 tit-dest orange">Gastronomia</h1>

                        <ul class="box-not">
                            <li class="col-lg-3 col-md-4 col-sm-6">
                                   
                                <a href="#" title="">
                                    <div class="col-lg-12 box-img">
                                        <img src="build/images/gastronomia/hamburguer.jpg" alt="" />
                                        <span class="tag tag-orange">Gastronomia</span>
                                    </div>

                                    <div class="col-lg-12 txt box-border">
                                        <h3 class="tit3">Philly burguer: hambúrguer caseiro com cebola caramelizada e pimentão</h3>
                                    </div>
                                </a>
                            </li>
                            
                            <li class="col-lg-3 col-md-4 col-sm-6">
                                <a href="#" title="">
                                    <div class="col-lg-12 box-img">
                                        <img src="build/images/gastronomia/pratos.jpg" alt="" />
                                        <span class="tag tag-orange">Gastronomia</span>
                                    </div>

                                    <div class="col-lg-12 txt box-border">
                                        <h3 class="tit3">Ensopado de carne ao vinho tinto com bolinhas de baguete</h3>
                                    </div>
                                </a>
                            </li>
                            
                            <li class="col-lg-3 col-md-4 hidden-sm">
                                <a href="#" title="">
                                    <div class="col-lg-12 box-img">
                                        <img src="build/images/gastronomia/bolo.jpg" alt="" />
                                        <span class="tag tag-orange">Gastronomia</span>
                                    </div>

                                    <div class="col-lg-12 txt box-border">
                                        <h3 class="tit3">Bolo de chocolate com recheio e cobertura de coco</h3>
                                    </div>
                                </a>
                            </li>
                            
                            <li class="col-lg-3 hidden-md hidden-sm">
                                <a href="#" title="">
                                    <div class="col-lg-12 box-img">
                                        <img src="build/images/gastronomia/salada-tipica.jpg" alt="" />
                                        <span class="tag tag-orange">Gastronomia</span>
                                    </div>

                                    <div class="col-lg-12 txt box-border">
                                        <h3 class="tit3">Salada de folhas com camarão e lascas de amêndoas</h3>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </section>

                    <!-- Meio Ambiente -->
                    <section class="row block-not">
                        <div class="col-lg-9 col-md-12 col-sm-12">
                            <h1 class="tit-dest darkgreen">Meio Ambiente</h1>

                            <ul class="box-not">
                                <li class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="#" title="">
                                        <div class="col-lg-12 box-img">
                                            <img src="build/images/meio-ambiente/arvores.jpg" alt="" />
                                            <span class="tag tag-darkgreen">Meio ambiente</span>
                                        </div>

                                        <div class="col-lg-12 txt box-border">
                                            <h3 class="tit3">Como imprimir gastando menos papel - e, assim, proteger o ambiente</h3>
                                        </div>
                                    </a>
                                </li>  
                                
                                <li class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="#" title="">
                                        <div class="col-lg-12 box-img">
                                           <img src="build/images/meio-ambiente/trigos.jpg" alt="" />
                                            <span class="tag tag-darkgreen">Meio ambiente</span>
                                        </div>

                                        <div class="col-lg-12 txt box-border">
                                            <h3 class="tit3">Como produzir mais comida sem agredir o meio ambiente</h3>
                                        </div>
                                    </a>
                                </li> 
                                
                                <li class="col-lg-4 col-md-4 hidden-sm">
                                    <a href="#" title="">
                                        <div class="col-lg-12 box-img">
                                           <img src="build/images/meio-ambiente/casa-ecologica.jpg" alt="" />
                                           <span class="tag tag-darkgreen">Meio ambiente</span>
                                        </div>

                                        <div class="col-lg-12 txt box-border">
                                            <h3 class="tit3">Descubra custos e benefícios de possuir uma casa ecológica</h3>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-lg-3 col-lg-offset-0 col-md-10 col-md-offset-1 box-m-publi">
                            <h5 class="tit-publi">Publicidade</h5>
                            <a href="#" title="">
                                <picture>
                                    <source media="(min-width: 1200px)" srcset="build/images/publi-1.jpg" />
                                    <source media="(min-width: 992px)" srcset="build/images/publi-1-m.jpg" />
                                    <source media="(min-width: 750px)" srcset="build/images/publi-1-m.jpg" />
                                    <img srcset="build/images/publi-1.jpg" alt="Banner" />
                                </picture>
                            </a>
                        </div>
                    </section>

                     <!-- Esporte e Turismo -->
                    <section class="row block-not">
                        <div class="col-lg-6 col-md-8 col-sm-6">
                            <h1 class="tit-dest green">Esporte</h1>

                            <ul class="box-not">
                                <li class="col-lg-6 col-md-6 col-sm-12">
                                    <a href="#" title="">
                                        <div class="col-lg-12 box-img">
                                            <img src="build/images/esporte/basquete.jpg" alt="" />
                                             <span class="tag tag-green">Esporte</span>
                                        </div>

                                        <div class="col-lg-12 txt box-border">
                                            <h3 class="tit3">Los Angeles Lakers confirma a contratação do armador Jeremy Lin </h3>
                                        </div>
                                    </a>
                                </li> 
                                   
                                <li class="col-lg-6 col-md-6 hidden-sm">
                                    <a href="#" title="">
                                        <div class="col-lg-12 box-img">
                                            <img src="build/images/esporte/volei.jpg" alt="" />
                                            <span class="tag tag-green">Esporte</span>
                                        </div>

                                        <div class="col-lg-12 txt box-border">
                                            <h3 class="tit3">Polônia e Brasil se enfrentam pela Liga Mundial</h3>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-lg-6 col-md-4 col-sm-6">
                            <h1 class="tit-dest lightgreen">Turismo</h1>

                            <ul class="box-not">
                               <li class="col-lg-6 col-md-12 col-sm-12">
                                    <a href="#" title="">
                                        <div class="col-lg-12 box-img">
                                            <img src="build/images/esporte/ciclismo.jpg" alt="" />
                                            <span class="tag tag-lightgreen">Turismo</span>
                                        </div>

                                        <div class="col-lg-12 txt box-border">
                                            <h3 class="tit3">Roteiros turísticos para ciclistas são atrações no Litoral Norte de Alagoas</h3>
                                        </div>
                                    </a>
                                </li> 
                                
                                <li class="col-lg-6 hidden-md hidden-sm">
                                    <a href="#" title="">
                                        <div class="col-lg-12 box-img">
                                            <img src="build/images/esporte/entrevista.jpg" alt="" />
                                            <span class="tag tag-lightgreen">Turismo</span>
                                        </div>

                                        <div class="col-lg-12 txt box-border">
                                            <h3 class="tit3">Picanha, cerveja e cantadas: estrangeiros revelam português que aprenderam</h3>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </section>


                    <!-- Entretenimento -->
                    <section class="row block-not">
                        <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                            <h1 class="tit-dest violet">Entretenimento</h1>
                            
                             <ul class="box-not">
                                <li class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <a href="#" title="">
                                        <div class="col-lg-12 box-img">
                                            <img src="build/images/musica/banda.jpg" alt="" />
                                            <span class="tag tag-violet">Música</span>
                                        </div>

                                        <div class="col-lg-12 txt box-border">
                                            <h3 class="tit3">Bandas e shows do Converse Rubber Tracks</h3>
                                        </div>
                                    </a>
                                </li>

                                <li class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <a href="#" title="">
                                        <div class="col-lg-12 box-img">
                                            <img src="build/images/musica/banda.jpg" alt="" />
                                            <span class="tag tag-violet">Música</span>
                                        </div>

                                        <div class="col-lg-12 txt box-border">
                                            <h3 class="tit3">Bandas e shows do Converse Rubber Tracks</h3>
                                        </div>
                                    </a>
                                </li>

                                <li class="col-lg-4 col-md-4 col-xs-12 hidden-sm">
                                    <a href="#" title="">
                                        <div class="col-lg-12 box-img">
                                            <img src="build/images/musica/banda.jpg" alt="" />
                                            <span class="tag tag-violet">Música</span>
                                        </div>

                                        <div class="col-lg-12 txt box-border">
                                            <h3 class="tit3">Bandas e shows do Converse Rubber Tracks</h3>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 box-newslatter">
                            <div class="row">
                                <div class="col-lg-12 col-md-4 col-sm-12">
                                    <h1 class="tit-dest lightblue">Newslatter</h1>
                                    <p class="txt-desc">Cadastre-se e receba as novidades do Norteando Você.</p>
                                </div>

                                <form class="col-lg-12 col-sm-12" role="form">
                                    <div class="col-lg-12 col-md-5 col-sm-5 form-group">
                                        <label for="nome" class="tit-form">Nome</label>
                                        <input type="email" class="form-control" id="nome" placeholder="Digite seu nome">
                                    </div>

                                    <div class="col-lg-12 col-md-5 col-sm-5 offset-1-custom form-group">
                                        <label for="email" class="tit-form">E-mail</label>
                                        <input type="email" class="form-control" id="email" placeholder="Digite seu email">
                                    </div>
                                    <div class="col-lg-12 col-md-1 col-sm-1">
                                        <button type="submit" class="btn bt-simples">Enviar</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </section>

                    <!-- Publicidade -->
                    <section class="row">
                        <a class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 publi-pad">
                            <h5 class="tit-publi">publicidade</h5>
                            <img src="build/images/banner-rdp.png" alt="" class="bord-full img-full"/>
                        </a>
                    </section>
                </article>
            </main>
        </div>
                            
                            
        
        <footer class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rdp">
            <div class="rdp-info">
                <div class="container">
                   <div class="row"> 
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h3 class="tit-default">Mais lidas</h3>

                        <ul class="list-p box-not">
                            <li class="col-lg-4 col-md-6 col-sm-4 col-xs-12">
                                <a href="#" title="">
                                    <div class="col-lg-12 col-xs-12 box-img">
                                        <img src="build/images/musica/banda2.jpg" alt="" />
                                         <div class="box-tag">
                                             <strong class="date">18/07</strong>
                                             <span class="tag-violet">Música</span>
                                         </div>
                                    </div>

                                    <div class="col-xs-12 txt not-rdp">
                                        <h3 class="tit4">Bandas e shows do Converse Rubber</h3>
                                    </div>
                                </a>
                            </li>
                            
                            <li class="col-lg-4 col-md-6 col-sm-4 col-xs-12">
                                <a href="#" title="">
                                    <div class="col-lg-12 box-img">
                                        <img src="build/images/musica/negro.jpg" alt="" />
                                        <div class="box-tag">
                                             <strong class="date">18/07</strong>
                                             <span class="tag-reset tag-violet">Música</span>
                                        </div>
                                    </div>

                                    <div class="col-md-12 txt not-rdp">
                                        <h3 class="tit4">O trailer de Jimi: All Is By My Side</h3>
                                    </div>
                                </a>
                            </li>

                            <li class="col-lg-4 hidden-md col-sm-4 col-xs-12">
                                <a href="#" title="">
                                    <div class="col-lg-12 col-md-12 box-img">
                                        <img src="build/images/musica/homem-do-cabelo-doido.jpg" alt="" />
                                        <div class="box-tag">
                                             <strong class="date">18/07</strong>
                                             <span class="tag-violet">Música</span>
                                         </div>
                                    </div>

                                    <div class="col-md-12 txt not-rdp">
                                        <h3 class="tit4">Fitz & the Tantrums from Daryl’s House</h3>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-12">
                        <div class="nav-info">
                            <ul class="menu-rdp col-lg-10 col-lg-offset-2 col-md-11 col-md-offset-1 col-sm-9 col-sm-offset-3">
                                <li><a href="#" title="Sobre">Sobre</a></li>
                                <li><a href="#" title="Anuncie">Anuncie</a></li>
                                <li><a href="#" title="Contato">Contato</a></li>
                                <li><a href="#" title="Expediente">Expediente</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-menu">
                            <ul class="menu-rdp">
                               <li class="col-lg-4 col-md-4 col-sm-3"><a href="#" title="Automobilismo">Automobilismo</a></li>
                               <li class="col-lg-4 col-md-4 col-sm-3"><a href="#" title="Coaching">Coaching</a></li>
                               <li class="col-lg-4 col-md-4 col-sm-3"><a href="#" title="Coluna">Coluna</a></li>
                               <li class="col-lg-4 col-md-4 col-sm-3"><a href="#" title="Ecomenico">Ecomenico</a></li>
                               <li class="col-lg-4 col-md-4 col-sm-3"><a href="#" title="Entretenimento">Entretenimento</a></li>
                               <li class="col-lg-4 col-md-4 col-sm-3"><a href="#" title="Esportes">Esportes</a></li>
                               <li class="col-lg-4 col-md-4 col-sm-3"><a href="#" title="Estilo & Beleza">Estilo & Beleza</a></li>
                               <li class="col-lg-4 col-md-4 col-sm-3"><a href="#" title="Gastronomia">Gastronomia</a></li>
                               <li class="col-lg-4 col-md-4 col-sm-3"><a href="#" title="Entretenimento">Entretenimento</a></li>
                               <li class="col-lg-4 col-md-4 col-sm-3"><a href="#" title="Esportes">Esportes</a></li>
                               <li class="col-lg-4 col-md-4 col-sm-3"><a href="#" title="Estilo & Beleza">Estilo & Beleza</a></li>
                               <li class="col-lg-4 col-md-4 col-sm-3"><a href="#" title="Gastronomia">Gastronomia</a></li> 
                            </ul>
                        </div> 
                        
                        <div class="col-lg-4 col-md-5 col-sm-4">
                            <ul class="midia">
                                <li><a href="#" class="face">Facebook</a></li>
                                <li><a href="#" class="twitter">Twitter</a></li>
                                <li><a href="#" class="g-plus">Google +</a></li>
                            </ul>
                        </div> 
                        
                        <div class="col-lg-1 col-lg-offset-7 col-md-1 col-md-offset-6 col-sm-1 col-sm-offset-7">
                            <a href="http://www.atratis.com.br" class="atratis">Atratis</a>
                        </div>          
                    </div>
                </div>
                </div>
            </div>
            
            <div class="row">
                <div class="cred-info col-lg-12">
                    <h2 class="logo-rdp">Norteando Logo Rdp</h2>
                </div>
            </div>
        </footer>
        
        
        <script src="build/js/main.min.js" async></script>
    </body>
</html>